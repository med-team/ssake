ssake (4.0.1-2) unstable; urgency=medium

  [ Charles Plessy ]
  * Team upload.
  * Build-depend on discount instead of Markown. Closes: #1073105

  [ Michael R. Crusoe ]
  * Standards-Version: 4.6.2 (routine-update)

 -- Michael R. Crusoe <crusoe@debian.org>  Wed, 03 Jul 2024 13:42:45 +0200

ssake (4.0.1-1) unstable; urgency=medium

  * Point watch file to github
  * Add salsa-ci.yml
  * New upstream version
  * Standards-Version: 4.6.0 (routine-update)
  * debhelper-compat 13 (routine-update)
  * Rules-Requires-Root: no (routine-update)
  * Use secure URI in Homepage field.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

 -- Andreas Tille <tille@debian.org>  Mon, 27 Sep 2021 13:55:43 +0200

ssake (4.0-3) unstable; urgency=medium

  * Use 2to3 to port to Python3
    Closes: #938561
  * Build-Depends: markdown instead of python-markdown
  * debhelper-compat 12
  * Standards-Version: 4.4.0
  * Respect DEB_BUILD_OPTIONS in override_dh_auto_test target
  * Remove trailing whitespace in debian/changelog
  * Set fields Upstream-Name in debian/copyright.
  * Remove obsolete fields Name from debian/upstream/metadata.

 -- Andreas Tille <tille@debian.org>  Thu, 05 Sep 2019 14:30:57 +0200

ssake (4.0-2) unstable; urgency=medium

  * Fix installation of tools
  * Fix link in examples, Depends: ssake
    Closes: #910414
  * Point Vcs fields to salsa.debian.org
  * Standards-Version: 4.2.1
  * Fix broken interpreter definitions
  * Avoid errors when trying to fix shebang line in shell scripts

 -- Andreas Tille <tille@debian.org>  Mon, 08 Oct 2018 09:04:43 +0200

ssake (4.0-1) unstable; urgency=medium

  * New upstream version

  [ Steffen Moeller ]
  * debian/upstream/metadata
    - yamllint cleanliness
    - added references to registries

  [ Andreas Tille ]
  * Adapt watch file to new tarball versioning
  * cme fix dpkg-control
  * debhelper 11
  * README is now delivered in md format
  * Remove potentially privacy breaching logo from readme

 -- Andreas Tille <tille@debian.org>  Sun, 18 Feb 2018 16:45:52 +0100

ssake (3.8.5-1) unstable; urgency=medium

  * New upstream version

  [ Steffen Moeller and Jon Ison ]
  * Introduced EDAM annotation to debian/upstream/edam

  [ Andreas Tille ]
  * Move packaging from SVN to Git
  * Standards-Version: 4.0.0 (no changes needed)
  * debhelper 10
  * d/rules: make use of DEB_SOURCE

 -- Andreas Tille <tille@debian.org>  Thu, 22 Jun 2017 10:58:29 +0200

ssake (3.8.4-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Wed, 27 Jan 2016 19:18:58 +0100

ssake (3.8.3-1) unstable; urgency=medium

  * New upstream version
  * cme fix dpkg-control
  * insert '#!/bin/sh' line into example scripts

 -- Andreas Tille <tille@debian.org>  Mon, 06 Jul 2015 21:19:41 +0200

ssake (3.8.2-1) unstable; urgency=medium

  * New upstream version
  * Fixed watch file
  * Prevent sending e-mail to upstream in case of successfull test

 -- Andreas Tille <tille@debian.org>  Sat, 19 Jul 2014 00:09:21 +0200

ssake (3.8.1-1) unstable; urgency=medium

  * New upstream version
    Closes: #735836
  * debian/control:
    - cme fix dpkg-control
    - debhelper 9
  * debian/rules: cdbs -> dh
  * debian/watch: fixed to report last version
  * add autopkgtest

 -- Andreas Tille <tille@debian.org>  Sat, 18 Jan 2014 16:02:43 +0100

ssake (3.8-2) unstable; urgency=low

  [ A. Costa ]
  * Corrected a typo in debian/ssake.1 (Closes: #650469).

  [ Charles Plessy ]
  * Depend on libperl4-corelibs-perl | perl (<< 5.12.3-7).
    Closes: #659428.

 -- Charles Plessy <plessy@debian.org>  Sat, 11 Feb 2012 22:13:43 +0900

ssake (3.8-1) unstable; urgency=low

  * New upstream version 30% faster than the previous release.  Additional
    assembly control has been implemented (-w) that limits the generation of
    low depth of coverage contigs.
  * Use Debhelper 8 (debian/control, debian/compat).
  * Corrected VCS URLs (debian/control).
  * Incremented Standards-Version to indicate conformance with Policy 3.9.2.
    (debian/control, changes needed.)
  * Implement rudimentary regression tests (debian/rules).

 -- Charles Plessy <plessy@debian.org>  Mon, 15 Aug 2011 14:10:18 +0900

ssake (3.7-1) unstable; urgency=low

  * New upstream version
  * Standards-Version: 3.9.1 (no changes needed)
  * debian/rules: reflect upstream renamed testdir

 -- Andreas Tille <tille@debian.org>  Thu, 25 Nov 2010 15:18:09 +0100

ssake (3.5-1) unstable; urgency=low

  * New upstream version
  * debian/source/format: Switched to source format 3.0 (quilt)
  * debian/control:
     - Standards-Version: 3.8.4 (no changes needed)
  * debian/rules
     - fixed cut-n-pasto

 -- Andreas Tille <tille@debian.org>  Thu, 24 Jun 2010 14:06:08 +0200

ssake (3.4-1) unstable; urgency=low

  * New upstream release.
     - Fixed a bug in PET routine.  User can now track read position and
       individual base coverage for reads *fully embedded* within contigs,
       using the -c option.
     - Version 3.4 exploits paired-end reads to explore possible contig merges
       within scaffolds (Consecutive contigs >= -z bases must overlap by -m
       bases or more).
  * debian/control:
    - Incremented Standards-Version to indicate conformance with Policy 3.8.2.
      (No changes needed.)
    - Using the Unicode ‘prime’ character for the 3′ direction (cosmetic).
  * Simplified debian/copyright according to latest discussions about the
    machine-readable format.

 -- Charles Plessy <plessy@debian.org>  Tue, 23 Jun 2009 13:58:49 +0900

ssake (3.2.1-2) unstable; urgency=low

  * debian/control:
    - ssake and ssake-exmamples now depend on ${misc:Depends}.
    - ssake-examples suggests ssake instead of depending.
    - Reduced redundancy in the description of ssake.
    - Gave a specific description to ssake-examples.
  * debian/ssake.docs, debian/reference: added BibTeX-formatted reference.
  * debian/copyright:
    - Corrected the identity of the upstream maintainer.
    - Points to the version 2 of the GNU GPL.
    - added myself to the Hall of Fame.

 -- Charles Plessy <plessy@debian.org>  Sat, 07 Mar 2009 13:42:47 +0900

ssake (3.2.1-1) unstable; urgency=low

  * Initial release (Closes: #506752)

 -- Andreas Tille <tille@debian.org>  Mon, 24 Nov 2008 13:12:38 +0100
